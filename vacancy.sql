-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 15 Agu 2019 pada 02.03
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vacancy`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `idadmin` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`idadmin`, `username`, `password`, `nama`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `applied`
--

CREATE TABLE `applied` (
  `idapplied` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `idjob` int(11) NOT NULL,
  `tgl_upload` date NOT NULL,
  `berkas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `applied`
--

INSERT INTO `applied` (`idapplied`, `iduser`, `idjob`, `tgl_upload`, `berkas`) VALUES
(1, 2, 6, '2019-05-05', 'aa.pdf'),
(2, 2, 6, '2019-03-04', '1563676630600.pdf'),
(3, 2, 6, '2019-11-21', '1563692528431.pdf'),
(4, 2, 6, '2019-08-12', '1565617078602.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `job`
--

CREATE TABLE `job` (
  `idjob` int(11) NOT NULL,
  `idperusahaan` int(11) NOT NULL,
  `tgl_awal` date NOT NULL,
  `tgl_akhir` date NOT NULL,
  `posisi` varchar(100) NOT NULL,
  `syarat` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `job`
--

INSERT INTO `job` (`idjob`, `idperusahaan`, `tgl_awal`, `tgl_akhir`, `posisi`, `syarat`, `gambar`) VALUES
(6, 1, '2019-07-01', '2019-07-02', 'Manager1', 'Apa saja', '1564196740340.png'),
(9, 1, '2019-07-05', '2019-07-06', 'Manager', 'qwerty', '1563642013982.png'),
(10, 2, '2019-07-11', '2019-07-13', 'Manager22', 'Apa saja', '1563646393621.png'),
(11, 1, '2019-07-04', '2019-07-11', 'aaaa', 'aser', '1563692678216.png'),
(12, 1, '2019-08-01', '2019-08-01', 'aaa', 'aa', '1565827273942.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perusahaan`
--

CREATE TABLE `perusahaan` (
  `idperusahaan` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nm_perusahaan` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `fax` varchar(12) NOT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `perusahaan`
--

INSERT INTO `perusahaan` (`idperusahaan`, `username`, `password`, `nm_perusahaan`, `alamat`, `email`, `telp`, `fax`, `logo`) VALUES
(1, 'aa', 'aa', 'Fedex', 'Jalan Bandung No 09', 'adityayogi711@gmail.com', '0857643222', '01234', 'job.png'),
(2, 'abc', 'abc', 'abc', 'abc', 'aa@g.com', '1234', '122', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` int(12) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `agama` enum('Islam','Hindu','Budha','Kristen Katholik','Kristen Protestan') NOT NULL,
  `jenis_kel` enum('Pria','Wanita') NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`iduser`, `username`, `password`, `fullname`, `alamat`, `email`, `nohp`, `tempat`, `tanggal_lahir`, `agama`, `jenis_kel`, `foto`) VALUES
(2, 'aa', 'aa', 'aa', 'aa', 'aa@g.com', 1234, 'Mojokerto', '2019-07-17', 'Hindu', 'Pria', NULL),
(3, 'abc', 'abc', 'aa', 'aa', 'adityayogi711@gmail.com', 1234, 'Mojokerto', '2019-07-26', 'Hindu', 'Pria', NULL),
(4, 'abcd', 'abcd', 'aaaa', 'aaaa', 'adityayogi711@gmail.com', 12345, 'MMMM', '2019-07-17', 'Islam', 'Pria', NULL);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_report`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_report` (
`idapplied` int(11)
,`username` varchar(100)
,`fullname` varchar(100)
,`email` varchar(100)
,`idjob` int(11)
,`idperusahaan` int(11)
,`posisi` varchar(100)
,`berkas` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in struktur untuk tampilan `v_report_detail`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_report_detail` (
`idapplied` int(11)
,`username_user` varchar(100)
,`fullname` varchar(100)
,`email` varchar(100)
,`idjob` int(11)
,`username` varchar(100)
,`nm_perusahaan` varchar(100)
,`posisi` varchar(100)
,`berkas` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_report`
--
DROP TABLE IF EXISTS `v_report`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_report`  AS  select `applied`.`idapplied` AS `idapplied`,`user`.`username` AS `username`,`user`.`fullname` AS `fullname`,`user`.`email` AS `email`,`job`.`idjob` AS `idjob`,`job`.`idperusahaan` AS `idperusahaan`,`job`.`posisi` AS `posisi`,`applied`.`berkas` AS `berkas` from ((`applied` join `user` on(`user`.`iduser` = `applied`.`iduser`)) join `job` on(`job`.`idjob` = `applied`.`idjob`)) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_report_detail`
--
DROP TABLE IF EXISTS `v_report_detail`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_report_detail`  AS  select `v_report`.`idapplied` AS `idapplied`,`v_report`.`username` AS `username_user`,`v_report`.`fullname` AS `fullname`,`v_report`.`email` AS `email`,`v_report`.`idjob` AS `idjob`,`perusahaan`.`username` AS `username`,`perusahaan`.`nm_perusahaan` AS `nm_perusahaan`,`v_report`.`posisi` AS `posisi`,`v_report`.`berkas` AS `berkas` from (`v_report` join `perusahaan` on(`perusahaan`.`idperusahaan` = `v_report`.`idperusahaan`)) ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`idadmin`);

--
-- Indeks untuk tabel `applied`
--
ALTER TABLE `applied`
  ADD PRIMARY KEY (`idapplied`),
  ADD KEY `FK_PELAMAR` (`iduser`),
  ADD KEY `FK_JOB` (`idjob`);

--
-- Indeks untuk tabel `job`
--
ALTER TABLE `job`
  ADD PRIMARY KEY (`idjob`),
  ADD KEY `FK_PERUSAHAAN` (`idperusahaan`);

--
-- Indeks untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  ADD PRIMARY KEY (`idperusahaan`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `idadmin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `applied`
--
ALTER TABLE `applied`
  MODIFY `idapplied` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `job`
--
ALTER TABLE `job`
  MODIFY `idjob` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `perusahaan`
--
ALTER TABLE `perusahaan`
  MODIFY `idperusahaan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `applied`
--
ALTER TABLE `applied`
  ADD CONSTRAINT `FK_JOB` FOREIGN KEY (`idjob`) REFERENCES `job` (`idjob`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_PELAMAR` FOREIGN KEY (`iduser`) REFERENCES `user` (`iduser`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
