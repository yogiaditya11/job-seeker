<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Top Navigation</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/AdminLTE/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="<?php echo base_url();?>pelamar" class="navbar-brand"><b>Job</b>Seeker</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="<?php echo base_url();?>pelamar">Home <span class="sr-only">(current)</span></a></li>
            <li><a href="<?php echo base_url();?>pelamar/goLamaran">Lamaran</a></li>
          </ul>
          <form class="navbar-form navbar-left" role="search">
            <div class="form-group">
              <input type="text" class="form-control" id="navbar-search-input" placeholder="Search">
            </div>
          </form>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="glyphicon glyphicon-user"> <?php echo $this->session->userdata("username"); ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- Menu Body -->
                <li class="user-body">
                  <div class="row">
                    <div class="pull-left">
                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                        <a href="<?php echo base_url();?>login/logoutPelamar" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </div>
                  <!-- /.row -->
                </li>
                <!-- Menu Footer-->
                
              </ul>
            </li>
          </ul>
        </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Halaman Utama
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active"><a href="#">Dashboard</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <center><h3 class="box-title">Nama Perusahaan</h3></center>
            </div>
             <div class="box-body">
                <center>
                    <img src="<?php echo base_url();?>assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <h4>Posisi</h4>
                    <h4>Persyaratan</h4>
                    <h4>Batas Aakhir</h4> 
                </center>
            </div>
          <!-- /.box-body -->
            <div class="box-footer">
            <center>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-ok-sign"> Apply</span></a>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-eye-open"> View</span></a>
                </center>
            </div>
        <!-- /.box-footer-->
        </div>
         <!-- /.box -->
        </div>

        <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <center><h3 class="box-title">Nama Perusahaan</h3></center>
            </div>
             <div class="box-body">
                <center>
                    <img src="<?php echo base_url();?>assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <h4>Posisi</h4>
                    <h4>Persyaratan</h4>
                    <h4>Batas Aakhir</h4> 
                </center>
            </div>
          <!-- /.box-body -->
            <div class="box-footer">
            <center>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-ok-sign"> Apply</span></a>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-eye-open"> View</span></a>
                </center>
            </div>
        <!-- /.box-footer-->
        </div>
         <!-- /.box -->
        </div>

        <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <center><h3 class="box-title">Nama Perusahaan</h3></center>
            </div>
             <div class="box-body">
                <center>
                    <img src="<?php echo base_url();?>assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <h4>Posisi</h4>
                    <h4>Persyaratan</h4>
                    <h4>Batas Aakhir</h4> 
                </center>
            </div>
          <!-- /.box-body -->
            <div class="box-footer">
            <center>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-ok-sign"> Apply</span></a>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-eye-open"> View</span></a>
                </center>
            </div>
        <!-- /.box-footer-->
        </div>
         <!-- /.box -->
        </div>

        <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <center><h3 class="box-title">Nama Perusahaan</h3></center>
            </div>
             <div class="box-body">
                <center>
                    <img src="<?php echo base_url();?>assets/AdminLTE/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                    <h4>Posisi</h4>
                    <h4>Persyaratan</h4>
                    <h4>Batas Aakhir</h4> 
                </center>
            </div>
          <!-- /.box-body -->
            <div class="box-footer">
            <center>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-success btn-flat"><span class="glyphicon glyphicon-ok-sign"> Apply</span></a>
                <a href="<?php echo base_url();?>perusahaan/goUpdateProfil" class="btn btn-info btn-flat"><span class="glyphicon glyphicon-eye-open"> View</span></a>
                </center>
            </div>
        <!-- /.box-footer-->
        </div>
         <!-- /.box -->
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
      </div>
      <strong>Copyright &copy; 2019 <a href="#"> Intan</a></strong> 
    </div>
    <!-- /.container -->
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>assets/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url();?>assets/AdminLTE/dist/js/demo.js"></script>
</body>
</html>
