<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pelamar extends CI_Model {

    public function getLowonganID($idjob)
    {
        $this->db->select('*');
        $this->db->from('job');
        $this->db->join('perusahaan', 'perusahaan.idperusahaan = job.idperusahaan', 'left');
        $this->db->where('idjob',$idjob);
        return $this->db->get()->result();
    }

    public function getLamaran()
    {
        $this->db->select('*');
        $this->db->from('applied');
        $this->db->join('job', 'job.idjob = applied.idjob', 'left');
        $this->db->join('user', 'user.iduser = applied.iduser', 'left');
        $this->db->where('username', $this->session->userdata('username'));
        return $this->db->get()->result();
    }

    public function insertUpload($data)
    {
        return $this->db->insert('applied', $data); 
    }

    public function get_logged_user_id()
	{
		$hasil = $this->db
						->select('iduser')
						->where('username', $this->session->userdata('username'))
						->limit(1)
						->get('user');
		if($hasil->num_rows() > 0){
			return $hasil->row()->iduser;
		}else{
			return 0;
		}
    }

}

/* End of file ModelName.php */

?>