<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_perusahaan extends CI_Model {

    public function getProfil()
    {
        $this->db->select('*');
        $this->db->from('perusahaan');
        $this->db->where('username', $this->session->userdata('username'));
        $query = $this->db->get();
        return $query->result();
    }

    public function input_lowongan($data,$table)
    {
        $this->db->insert($table, $data);
    }

    public function update_lowongan($where,$table)
    {
        return $this->db->get_where($table,$where);
    }

    public function edit_lowongan($where,$data,$table)
    {
        $this->db->where($where);
		$this->db->update($table,$data);
    }

    public function deleteData($where,$table)
    {
        $this->db->where($where);
	    $this->db->delete($table);
    }

    public function getApply()
    {
        $this->db->select('*');
        $this->db->from('v_report_detail');
        $this->db->where('username', $this->session->userdata('username'));
        $query = $this->db->get();
        return $query->result();
    }

    public function get_logged_user_id()
	{
		$hasil = $this->db
						->select('idperusahaan')
						->where('username', $this->session->userdata('username'))
						->limit(1)
						->get('perusahaan');
		if($hasil->num_rows() > 0){
			return $hasil->row()->idperusahaan;
		}else{
			return 0;
		}
    }
    
    public function getDataLowongan()
    {
        $this->db->select('b.idjob, a.idperusahaan ,a.nm_perusahaan, a.username, b.tgl_awal, b.tgl_akhir, b.posisi, b.syarat, b.gambar');
        $this->db->from('job b');
        $this->db->join('perusahaan a', 'a.idperusahaan = b.idperusahaan');
        $this->db->where('a.username', $this->session->userdata('username'));
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataLowongan1()
    {
        $this->db->select('b.idjob, a.nm_perusahaan, a.username, a.telp, a.alamat, a.email, a.fax, b.tgl_awal, b.tgl_akhir, b.posisi, b.gaji, b.syarat, b.gambar, a.logo');
        $this->db->from('job b');
        $this->db->join('perusahaan a', 'a.idperusahaan = b.idperusahaan', 'left');
        $query = $this->db->get();
        return $query->result();
    }

    public function insertUpload($data)
    {
        return $this->db->insert('job', $data); 
    }

    function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}	

    public function getReport()
    {
        $this->db->select('*');
        $this->db->from('applied');
        $this->db->join('job', 'job.idjob = applied.idjob', 'left');
        $this->db->join('user', 'user.iduser = applied.iduser', 'left');
        $this->db->where('username', $this->session->userdata('username'));
        return $this->db->get()->result();
    }

    public function getReportTahun()
    {
        $this->db->select('COUNT(*) as jumlah_lamaran, YEAR(tgl_upload) as tahun');
        $this->db->from('applied');
        $this->db->join('user', 'user.iduser = applied.iduser', 'left');
        $this->db->where('username', $this->session->userdata('username'));
        $this->db->group_by('YEAR(tgl_upload)');
        return $this->db->get()->result();
    }
}

/* End of file ModelName.php */

?>