<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pelamar extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $this->load->model('model_perusahaan');
        $this->load->model('model_pelamar');
        
    }
    
    public function index()
    {
        $data['lowongan'] = $this->model_perusahaan->getDataLowongan1();
        $this->load->view('pelamar/home',$data);
    }

    public function goLamaran()
    {
        $data['lamaran'] = $this->model_pelamar->getLamaran();
        $this->load->view('pelamar/lamaran',$data);
    }

    public function goDetailJob($idjob)
    {
        //$data['detail'] = $this->model_pelamar->getLowonganID($idjob)->row_array();
        $data['detail'] = $this->model_pelamar->getLowonganID($idjob);
        $this->load->view('pelamar/detail_job',$data);
    }

    public function goApply()
    {
        $this->load->view('pelamar/apply_cv');
    }

    public function insertData()
    {
        $data = array(
            // 'no_daftar' => $this->input->post('no_daftar'),
            'iduser' => $this->model_pelamar->get_logged_user_id(),
            'tgl_upload' => date('Y-m-d'),
            'idjob' => $this->input->post('idjob'),
            
		);
		if (!empty($_FILES['berkas']['name'])) {
			$upload = $this->_do_upload();
			$data['berkas'] = $upload;
		}
		$this->model_pelamar->insertUpload($data);
		redirect('pelamar', $data);
    }

    private function _do_upload()
	{
		$config['upload_path'] 		= './upload/cv/';
		$config['allowed_types'] 	= 'pdf';
		$config['max_size'] 		= 2048;
		$config['max_widht'] 		= 1000;
		$config['max_height']  		= 1000;
		$config['file_name'] 		= round(microtime(true)*1000);
 
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('berkas')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('',''));
			redirect('pelamar');
		}
		return $this->upload->data('file_name');
	}

}

/* End of file Pelamar.php */

?>