<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');    
    }
    ##Pelamar
    public function index()
    {
        $this->load->view('login');
    }

    public function goRegisPelamar()
    {
        $this->load->view('register_pelamar');
    }

    public function actRegisPelamar()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $fullname = $this->input->post('fullname');
        $email = $this->input->post('email');
        $nohp = $this->input->post('nohp');
        $tmp = $this->input->post('tempat');
        $tgl = $this->input->post('tanggal_lahir');
        $agama = $this->input->post('agama');
        $jk = $this->input->post('jenis_kel');
        $alamat = $this->input->post('alamat');
        
        $data_pelamar = array(
            'username' => $user,
            'password' => $pass,
            'fullname' => $fullname,
            'alamat' => $alamat,
            'email' => $email,
            'nohp' => $nohp,
            'tempat' => $tmp,
            'tanggal_lahir' => $tgl,
            'agama' => $agama,
            'jenis_kel' => $jk
        );
        $this->model_login->input_perusahaan($data_pelamar,'user');
        redirect('login');
    }

    public function actLoginPelamar()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => $pass
        );
        $cek = $this->model_login->cekLogin("user",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("pelamar");
        } else {
            echo "Username atau password salah";
        }
    }

    public function logoutPelamar(){
        $this->session->sess_destroy();
        redirect('login');
    }

    #perusahaan
    public function LoginPerusahaan()
    {
        $this->load->view('login_perusahaan');
    }

    public function Login2()
    {
        $this->load->view('login2');
    }

    public function goRegisPerusahaan()
    {
        $this->load->view('register_perusahaan');
    }

    public function actRegisPerusahaan()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $nm_perusahaan = $this->input->post('nm_perusahaan');
        $alamat = $this->input->post('alamat');
        $telp = $this->input->post('telp');
        $email = $this->input->post('email');
        $fax = $this->input->post('fax');

        $data_perusahaan = array(
            'username' => $user,
            'password' => $pass,
            'nm_perusahaan' => $nm_perusahaan,
            'alamat' => $alamat,
            'email' => $email,
            'telp' => $telp,
            'fax' => $fax,
        );
        $this->model_login->input_perusahaan($data_perusahaan,'perusahaan');
        redirect('login/LoginPerusahaan');
    }

    public function actLoginPerusahaan()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => $pass
        );
        $cek = $this->model_login->cekLogin("perusahaan",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("perusahaan/home");
        } else {
            echo "Username atau password salah";
        }
    }

    public function actLogin2()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => md5($pass)
        );
        $cek = $this->model_login->cekLogin("admin",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("admin");
        } else {
            echo "Username atau password salah";
        }
    }

    public function logoutPerusahaan(){
        $this->session->sess_destroy();
        redirect('login/LoginPerusahaan');
    }

    public function logoutAdmin(){
        $this->session->sess_destroy();
        redirect('login/login2');
    }



}

/* End of file Login.php */

?>