<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Perusahaan extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect('login/LoginPerusahaan');
        }
        $this->load->model('model_perusahaan');
        
    }

    public function goReport()
    {
        $data['apply'] = $this->model_perusahaan->getApply();
        $this->load->view('perusahaan/report',$data);   
    }
    public function home()
    {
        $this->load->view('perusahaan/home');
    }

    function goProfil(){
        $data['profil'] = $this->model_perusahaan->getProfil();
        $this->load->view('perusahaan/profil',$data); 
    }

    function goUpdateProfil($idperusahaan){
        $where = array('idperusahaan' => $idperusahaan);
	    $data['profil'] = $this->model_perusahaan->update_lowongan($where,'perusahaan')->result();
        $this->load->view('perusahaan/update-profil',$data); 
    }

    public function actUpdateProfil()
    {
        $idperusahaan = $this->input->post('idperusahaan');
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $nm_perusahaan = $this->input->post('nm_perusahaan');
        $alamat = $this->input->post('alamat');
        $email = $this->input->post('email');
        $telp = $this->input->post('telp');
        $fax = $this->input->post('fax');
        
        $data = array(
            'username' => $username,
            'password' => $password,
            'nm_perusahaan' => $nm_perusahaan,
            'alamat' => $alamat,
            'email' => $email,
            'telp' => $telp,
            'fax' => $fax
        );

        $where = array(
            'idperusahaan' => $idperusahaan
        );

        $this->model_perusahaan->edit_lowongan($where, $data, 'perusahaan');
        redirect('perusahaan/goProfil');
    }

    public function goLowongan()
    {   
        $data['lowongan'] = $this->model_perusahaan->getDataLowongan();
        $this->load->view('perusahaan/lowongan',$data);
    }

    public function goAddLowongan()
    {
        $this->load->view('perusahaan/tambah-lowongan');
    }

    public function actAddLowongan()
    {
        $tgl_awal = $this->input->post('tgl_awal');
        $akhir = $this->input->post('tgl_akhir');
        $posisi = $this->input->post('posisi');
        // $gaji = $this->input->post('gaji');
        $syarat = $this->input->post('syarat');
        $gambar = $this->model_perusahaan->_uploadImage();
        
        $dt_lowongan = array(
            'idperusahaan' => $this->model_perusahaan->get_logged_user_id(),
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $akhir,
            'posisi' => $posisi,
            'syarat' => $syarat,
            // 'gaji' => $gaji,
            'gambar' => $gambar
        );
        
        $this->model_perusahaan->input_lowongan($dt_lowongan,'job');
        redirect('perusahaan/goLowongan');
    }

    public function goEditLowongan($idjob)
    {
        $where = array('idjob' => $idjob);
		$data['lowongan'] = $this->model_perusahaan->update_lowongan($where,'job')->result();
		$this->load->view('perusahaan/edit-lowongan',$data);
    }

    public function actUpdateLowongan()
    {
        $idjob = $this->input->post('idjob');
        $tgl_awal = $this->input->post('tgl_awal');
        $akhir = $this->input->post('tgl_akhir');
        $posisi = $this->input->post('posisi');
        $gaji = $this->input->post('gaji');
        $syarat = $this->input->post('syarat');

        $data = array(
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $akhir,
            'posisi' => $posisi,
            'gaji' => $gaji,
            'syarat' => $syarat
        );

        $where = array(
            'idjob' => $idjob
        );

        $this->model_perusahaan->edit_lowongan($where,$data,'job');
        redirect('perusahaan/goLowongan');
    }

    public function goDeleteLowongan($idjob)
    {
        $where = array('idjob' => $idjob);
        $this->model_perusahaan->deleteData($where,'job');
        redirect('perusahaan/goLowongan'); 
    }

    public function index(){
        //$data['perusahaan'] = $this->model_perusahaan->getProfil()->result();
		$this->load->view('perusahaan/profil');
    }

    public function insertData()
    {
        $data = array(
            'idperusahaan' => $this->model_perusahaan->get_logged_user_id(),
            'tgl_awal' => $this->input->post('tgl_awal'),
            'tgl_akhir' => $this->input->post('tgl_akhir'),
            'posisi' => $this->input->post('posisi'),
            // 'gaji' => $this->input->post('gaji'),
            'syarat' => $this->input->post('syarat'),
		);
		if (!empty($_FILES['gambar']['name'])) {
			$upload = $this->_do_upload();
			$data['gambar'] = $upload;
		}
		$this->model_perusahaan->insertUpload($data);
		redirect('perusahaan/goLowongan', $data);
    }

    public function edittData()
    {
        $data = array(
            'idperusahaan' => $this->model_perusahaan->get_logged_user_id(),
            'tgl_awal' => $this->input->post('tgl_awal'),
            'tgl_akhir' => $this->input->post('tgl_akhir'),
            'posisi' => $this->input->post('posisi'),
            // 'gaji' => $this->input->post('gaji'),
            'syarat' => $this->input->post('syarat'),
        );
		if (!empty($_FILES['gambar']['name'])) {
			$upload = $this->_do_upload();
			$data['gambar'] = $upload;
        }
        
        $where = array(
            'idjob' => $this->input->post('idjob'),
        );

		$this->model_perusahaan->edit_lowongan($where, $data, 'job');
		redirect('perusahaan/goLowongan', $data);
    }

    private function _do_upload()
	{
		$config['upload_path'] 		= './upload/job/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size'] 		= 2048;
		$config['max_widht'] 		= 1000;
		$config['max_height']  		= 1000;
		$config['file_name'] 		= round(microtime(true)*1000);
 
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('gambar')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('',''));
			redirect('perusahaan/goLowongan');
		}
		return $this->upload->data('file_name');
    }
    
    public function getReportTahunan()
    {
        $data['tahun'] = $this->model_perusahaan->getReportTahun();
        $this->load->view('perusahaan/reportTahun',$data);    
    }
}

/* End of file Controllername.php */

?>