<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_admin');
        
    }
    
    public function index()
    {
        $this->load->view('admin/home');
    }

    public function goUser()
    {
        $data['perusahaan'] = $this->model_admin->getData1()->result();
        $this->load->view('admin/perusahaan', $data); 
    }
    

    public function goUser1()
    {
        $data['pelamar'] = $this->model_admin->getData()->result();
        $this->load->view('admin/pelamar', $data);
    }

}

/* End of file Admin.php */

?>